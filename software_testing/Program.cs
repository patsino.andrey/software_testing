﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

class Program
{
    private const string Url = "http://svyatoslav.biz/testlab/wt/";

    static void Main(string[] args)
    {
        IWebDriver driver = new ChromeDriver();
        driver.Navigate().GoToUrl(Url);
        BannersCheck(driver);
        CopyrightCheck(driver);
        IsBlanc(driver);
        LowMassCheck(driver);
        FormCheck(driver);
        RangeCheck(driver);
        CheckFormat(driver);
        driver.Quit();
    }

    static void BannersCheck(IWebDriver driver)
    {
        Console.WriteLine("Test #1. The home page contains the words \"menu\" and \"banners\":");
        bool hasMenu = driver.PageSource.Contains("menu");
        bool hasBanners = driver.PageSource.Contains("banners");
        if (hasMenu && hasBanners)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Passed");
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Failed");
        }
        Console.ResetColor();
        Console.WriteLine();
    }

    static void CopyrightCheck(IWebDriver driver)
    {
        Console.WriteLine("Test #2. The bottom cell of the table contains the text «CoolSoft by Somebody»:");
        string pageSource = driver.PageSource;
        if (pageSource.Contains("© CoolSoft by Somebody"))
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Passed");
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Failed");
        }
        Console.ResetColor();
        Console.WriteLine();
    }

    static void IsBlanc(IWebDriver driver)
    {
        Console.WriteLine("Test #3. All fields is blanc by default:");
        IWebElement height = driver.FindElement(By.Name("height"));
        IWebElement weight = driver.FindElement(By.Name("weight"));
        IWebElement name = driver.FindElement(By.Name("name"));
        IWebElement gender = driver.FindElement(By.Name("gender"));
        bool areTextFieldsEmpty = string.IsNullOrEmpty(height.GetAttribute("value"))
            && string.IsNullOrEmpty(weight.GetAttribute("value"))
            && string.IsNullOrEmpty(name.GetAttribute("value"))
            && !gender.Selected;
        if (areTextFieldsEmpty)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Passed");
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Failed");
        }
        Console.ResetColor();
        Console.WriteLine();
    }

    static void LowMassCheck(IWebDriver driver)
    {
        Console.WriteLine("Test #4. After filling the \"Height\" field with \"50\" and weight with \"3\" " +
            "and submitting the form, the form disappears, and instead the message \"Слишком большая масса тела\" appears:");
        IWebElement height = driver.FindElement(By.Name("height"));
        IWebElement weight = driver.FindElement(By.Name("weight"));
        IWebElement name = driver.FindElement(By.Name("name"));
        IWebElement gender = driver.FindElement(By.Name("gender"));
        height.SendKeys("50");
        weight.SendKeys("3");
        name.SendKeys("Evgeny Nanikov");
        gender.Click();
        IWebElement submitButton = driver.FindElement(By.XPath("//input[@type='submit']"));
        submitButton.Click();
        bool hasError = driver.PageSource.Contains("Слишком большая масса тела");
        if (hasError)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Passed");
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Failed");
        }
        Console.ResetColor();
        Console.WriteLine();
        driver.Navigate().Back();
    }

    static void FormCheck(IWebDriver driver)
    {Console.WriteLine("Test #5. The main page of the application contains a form with three text fields:");
        bool hasForm = driver.FindElement(By.TagName("form")).Displayed;
        if (hasForm)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Passed");
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Failed");
        }
        Console.ResetColor();
        Console.WriteLine();
    }

    static void RangeCheck(IWebDriver driver)
    {
        IWebElement height = driver.FindElement(By.Name("height"));
        IWebElement weight = driver.FindElement(By.Name("weight"));
        IWebElement submitButton = driver.FindElement(By.XPath("//input[@type='submit']"));
        height.Clear();
        weight.Clear();
        submitButton.Click();
        bool hasHightError = driver.PageSource.Contains("Рост должен быть в диапазоне 50-300 см.");
        bool hasWeightError = driver.PageSource.Contains("Вес должен быть в диапазоне 3-500 кг.");
        Console.WriteLine("Test #6. Checking message about invalid height/weight values:");
        if (hasHightError && hasWeightError)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Passed");
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Failed");
        }
        Console.ResetColor();
        Console.WriteLine();
    }

    static void CheckFormat(IWebDriver driver)
    {
        Console.WriteLine("Test #7. Main page contains date in «DD.MM.YYYY» format:");
        string currentDate = DateTime.Now.ToString("dd.MM.yyyy");
        bool isDatePresent = driver.PageSource.Contains(currentDate);
        if (isDatePresent)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Passed");
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Failed");
        }
        Console.ResetColor();
        Console.WriteLine();
    }
}